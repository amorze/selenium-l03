package com.mycompany.l03.model;

import com.mycompany.l03.utils.*;
import com.mycompany.l03.data.Credentials;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    public WebDriver driver;

    @BeforeMethod
    public void initData() {
        driver = new ChromeDriver();
        driver.get("http://cw07529-wordpress.tw1.ru/my-account/");
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void stop() {
        driver.quit();
    }

    @BeforeClass
    public void createUser() throws InterruptedException {
        driver = new ChromeDriver();
        driver.get("http://cw07529-wordpress.tw1.ru/wp-admin/user-new.php");
        AdminUtils.loginAsAdmin(driver, Credentials.adminName, Credentials.adminPassword);

        String password = AdminUtils.createUser(driver, Credentials.userName);
        Credentials.setUserPassword(password);

        Thread.sleep(5000);
        driver.quit();
    }
}


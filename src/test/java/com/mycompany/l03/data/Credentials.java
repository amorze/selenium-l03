package com.mycompany.l03.data;

import com.mycompany.l03.utils.StringUtils;

public class Credentials {

    public static final String adminName = "admin";
    public static final String adminPassword = "kCb1hRpr";

    public static final String userName = StringUtils.getRandomString(8);
    static String userPassword;

    public static String getUserPassword() {
        return userPassword;
    }

    public static void setUserPassword(String userPassword) {
        Credentials.userPassword = userPassword;
    }
}

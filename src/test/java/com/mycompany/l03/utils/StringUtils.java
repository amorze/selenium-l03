package com.mycompany.l03.utils;

import org.apache.commons.lang.RandomStringUtils;

public class StringUtils {

    public static String getRandomString(int length) {
        return RandomStringUtils.random(length, true, true);
    }

}
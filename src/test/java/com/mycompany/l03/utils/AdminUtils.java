package com.mycompany.l03.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AdminUtils {

    public static void loginAsAdmin(WebDriver driver, String adminName, String adminPassword) {
        driver.manage().window().maximize();

        //login field
        WebElement userNameInput = driver.findElement(By.xpath("//input[@id='user_login']"));
        userNameInput.sendKeys(adminName);

        //password field
        WebElement passwordInput = driver.findElement(By.xpath("//input[@id='user_pass']"));
        passwordInput.sendKeys(adminPassword);

        //click login button
        WebElement loginButton = driver.findElement(By.xpath("//input[@id='wp-submit']"));
        loginButton.click();
    }

    public static String createUser(WebDriver driver, String name) {

        WebElement userName = driver.findElement(By.xpath("//input[@id='user_login']"));
        userName.sendKeys(name);

        WebElement userEmail = driver.findElement(By.xpath("//input[@id='email']"));
        userEmail.sendKeys(name + "@testmail.ru");

        WebElement showPassButtondriver = driver.findElement(By.xpath("//button[@class='button wp-generate-pw hide-if-no-js']"));
        showPassButtondriver.click();

        WebElement passwordField = driver.findElement(By.xpath("//input[@id='pass1']"));
        String password = passwordField.getAttribute("data-pw");

        WebElement commitButton = driver.findElement(By.xpath("//input[@id='createusersub']"));
        commitButton.click();

        return password;
    }

}
